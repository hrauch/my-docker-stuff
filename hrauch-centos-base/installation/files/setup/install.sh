#! /bin/bash
#set -o errexit
set -e
set -o pipefail
set -x

#export DEBIAN_FRONTEND=noninteractive
echo "GIT_COMMIT = ${GIT_COMMIT}"
export TZ=Europe/Berlin

echo "Europe/Berlin" > /etc/TZ
echo "TZ=Europe/Berlin" >> /etc/environment

# dirmngr: required for GnuPG
# python3-pip, python3-setuptools: required for chaperone
# locales: required for locale-gen
# binutils: strings, nm cmds
# dnsutils: host, dig cmds
# procps, psmisc: /proc fs support
# net-tools: basic networking utils (netstat and such)
# curl: HTTP downloads
# less: pager
# jq: JSON parser for cmd line (useful for evaluating "docker inspect" output)
# file: determine file types
# vim-tiny,joe,nano: for editing text files
# git: needed as a docker dependency and also within Jenkins slave image
#apt-get -qq -y update && apt-get -qq -y --no-install-recommends install \
yum -y update
yum -y install yum-utils
#yum -y groupinstall development
#yum -y install https://centos7.iuscommunity.org/ius-release.rpm
#yum -y install python36u python36u-pip python36u-devel
yum install -y \
locales \
bash-completion \
zip \
unzip \
bzip2 \
procps \
psmisc \
ca-certificates \
curl \
wget \
less \
jq \
file \
vim-tiny \
joe \
nano \
sudo \
openssh-client \
git

yum clean all
rm -rf /var/cache/yum

locale
#locale-gen en_US.UTF-8
#locale-gen de_DE.UTF-8
#localectl ...

#python3.6 -V
#pip3.6 --version

# upgrade pip using pip itself ;-)
#pip3.6 install -U pip
#pip3.6 --version
#pip3.6 install chaperone
#pip3.6 install sslyze
#sslyze --version
    
echo $GIT_COMMIT > /etc/docker_image_version

chmod -c 666 /etc/hosts

echo "creating .bashrc for root user"
cat /setup-baseimage/dot-bashrc > /root/.bashrc

#echo "adding /etc/hosts entries"
# /tmp will contain the *writable* copy of /etc/hosts
#cp -av /etc/hosts /tmp
#cat /setup-baseimage/hosts >> /tmp/hosts
# modifying /etc/hosts directly prior from within a CMD/ENTRYPOINT doesn't
# work
# use ugly hack below => shamelessly stolen from
# http://jasonincode.com/customizing-hosts-file-in-docker/
# (alternative would be to set up a chaperone cfg file, but that's
# a bit overkill IMHO just for adding a few hosts ;-) to /etc/hosts)
#/bin/bash -c "cat /setup-baseimage/hosts >> /etc/hosts"
#mkdir -p /lib-override
#cp /lib/x86_64-linux-gnu/libnss_files-2.23.so /lib-override
#cp /usr/glibc-compat/lib/libnss_files-2.23.so /lib-override
# perl instead of sed for inplace editing (alternative would be ssed)
# IMPORTANT: Substituted path must be of exact same length, since we're
# modifying a text string inside a binary (.so file) here!
# (better be safe than sorry and patch *both* C libs)
#perl -pi -e 's:/etc/hosts:/tmp/hosts:g' /lib-override/libnss_files-2.23.so
#export LD_LIBRARY_PATH=/lib-override

#echo "Copying chaperone config files"
#mkdir /etc/chaperone.d
#cp -av /setup-baseimage/*.conf /etc/chaperone.d

#echo "Copying Docker certs to /etc/docker/certs.d"
mkdir -p /etc/docker
#cp -av /setup-baseimage/etc/docker/* /etc/docker
ls -l /etc/docker

#echo "installing client ssh config files"
#mkdir ~/.ssh
#chmod -c 700 ~/.ssh
#cp -v /setup-baseimage/*rsa* ~/.ssh
#cp -v /setup-baseimage/config ~/.ssh
#cp -v /setup-baseimage/authorized_keys ~/.ssh

# see locale cmd for a complete list of locale related environment variables
# why not simply use LC_ALL? Because we still want english (error) messages
LANG=en_US.UTF-8
LC_TIME=de_DE.UTF-8
LC_NAME=de_DE.UTF-8
LC_ADDRESS=de_DE.UTF-8
LC_NUMERIC=de_DE.UTF-8
LC_MEASUREMENT=de_DE.UTF-8
LC_MONETARY=de_DE.UTF-8
LC_TELEPHONE=de_DE.UTF-8
LC_PAPER=de_DE.UTF-8
LC_IDENTIFICATION=de_DE.UTF-8
LC_COLLATE=de_DE.UTF-8
LC_CTYPE=de_DE.UTF-8

echo "
LANG=en_US.UTF-8
LC_TIME=de_DE.UTF-8
LC_NAME=de_DE.UTF-8
LC_ADDRESS=de_DE.UTF-8
LC_NUMERIC=de_DE.UTF-8
LC_MEASUREMENT=de_DE.UTF-8
LC_MONETARY=de_DE.UTF-8
LC_TELEPHONE=de_DE.UTF-8
LC_PAPER=de_DE.UTF-8
LC_IDENTIFICATION=de_DE.UTF-8
LC_COLLATE=de_DE.UTF-8
LC_CTYPE=de_DE.UTF-8
" >> /etc/environment

export LANG \
LC_TIME \
LC_NAME \
LC_ADDRESS \
LC_NUMERIC \
LC_MEASUREMENT \
LC_MONETARY \
LC_TELEPHONE \
LC_PAPER \
LC_IDENTIFICATION \
LC_COLLATE \
LC_CTYPE
