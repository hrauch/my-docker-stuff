#!/bin/bash
set -e
set -o pipefail
set -x

[ -z $GIT_COMMIT ] && exit 1 || echo "GIT_COMMIT = ${GIT_COMMIT}"

[ -f /opt/rh/rh-ruby23/enable ] && . /opt/rh/rh-ruby23/enable || exit 1

gem install gauntlt --no-rdoc --no-ri
gauntlt --version

cd /usr/local

tar xvfz dirb222.tar.gz > /dev/null
cd dirb222
chmod 755 ./configure
./configure
make
ln -s /usr/local/dirb222/dirb /usr/local/bin/dirb

export DIRB_WORDLISTS=/usr/local/dirb222/wordlists
#dirb --help

echo $GIT_COMMIT > /etc/docker_image_version
