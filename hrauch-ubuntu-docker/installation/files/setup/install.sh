#! /bin/bash
set -x
for i in "$*"; do
   $i
done

GPG_KEYSERVER="ha.pool.sks-keyservers.net"
#GPG_KEYSERVER="keyserver.ubuntu.com"

# DOCKER_BRANCH=stable
# DOCKER_VERSION=18.03.1-ce

echo "
TINI_VERSION=${TINI_VERSION}
GOSU_VERSION=${GOSU_VERSION}
DOCKER_BUCKET=${DOCKER_BUCKET}
DOCKER_BRANCH=${DOCKER_BRANCH}
DOCKER_VERSION=${DOCKER_VERSION}
DOCKER_SHA256=${DOCKER_SHA256}
"

export DEBIAN_FRONTEND=noninteractive
export TZ=Europe/Berlin

echo "Europe/Berlin" > /etc/TZ
echo "TZ=Europe/Berlin" >> /etc/environment

#echo "Contents of /etc/hosts:"
#cat /etc/hosts

#echo "Contents of /etc/resolv.conf:"
#cat /etc/resolv.conf


# required for apt-add-repository
# software-properties-common: required for 'apt-add-repository' tool
# dirmngr: required by GnuPG
# python3-pip, python3-setuptools: required for chaperone
# locales: required for locale-gen
# binutils: strings, nm cmds
# dnsutils: host, dig cmds
# procps, psmisc: /proc fs support
# net-tools: basic networking utils (netstat and such)
# curl: HTTP downloads
# less: pager
# jq: JSON parser for cmd line (useful for evaluating "docker inspect" output)
# file: determine file types
# vim-tiny,joe,nano: for editing text files
# git: needed as a docker dependency and also within Jenkins slave image
apt-get -qq -y update && apt-get -qq -y --no-install-recommends install \
dirmngr \
software-properties-common \
apt-transport-https \
binutils \
dnsutils \
python3-pip \
python3-setuptools \
locales \
make \
iputils-ping \
bash-completion \
zip \
unzip \
bzip2 \
procps \
psmisc \
daemon \
net-tools \
ca-certificates \
curl \
idn2 \
less \
jq \
file \
vim-tiny \
nano \
joe \
sudo \
openssh-client \
git

locale-gen en_US.UTF-8
locale-gen de_DE.UTF-8
dpkg-reconfigure locales

# upgrade pip using pip itself ;-)
pip3 install -U pip

pip3 install -U chaperone

pip3 install -U docker-compose

chmod -c 666 /etc/hosts

echo "creating .bashrc for root user"
cat /setup-baseimage/dot-bashrc > /root/.bashrc

#echo "Copying Docker certs to /etc/docker/certs.d"
mkdir -p /etc/docker
#cp -av /setup-baseimage/etc/docker/* /etc/docker
ls -l /etc/docker

#echo "installing client ssh config files"
#mkdir ~/.ssh
#chmod -c 700 ~/.ssh
#cp -v /setup-baseimage/*rsa* ~/.ssh
#cp -v /setup-baseimage/config ~/.ssh
#cp -v /setup-baseimage/authorized_keys ~/.ssh

# see locale cmd for a complete list of locale related environment variables
# why not simply use LC_ALL? Because we still want english (error) messages
LANG=en_US.UTF-8
LC_TIME=de_DE.UTF-8
LC_NAME=de_DE.UTF-8
LC_ADDRESS=de_DE.UTF-8
LC_NUMERIC=de_DE.UTF-8
LC_MEASUREMENT=de_DE.UTF-8
LC_MONETARY=de_DE.UTF-8
LC_TELEPHONE=de_DE.UTF-8
LC_PAPER=de_DE.UTF-8
LC_IDENTIFICATION=de_DE.UTF-8
LC_COLLATE=de_DE.UTF-8
LC_CTYPE=de_DE.UTF-8

echo "
LANG=en_US.UTF-8
LC_TIME=de_DE.UTF-8
LC_NAME=de_DE.UTF-8
LC_ADDRESS=de_DE.UTF-8
LC_NUMERIC=de_DE.UTF-8
LC_MEASUREMENT=de_DE.UTF-8
LC_MONETARY=de_DE.UTF-8
LC_TELEPHONE=de_DE.UTF-8
LC_PAPER=de_DE.UTF-8
LC_IDENTIFICATION=de_DE.UTF-8
LC_COLLATE=de_DE.UTF-8
LC_CTYPE=de_DE.UTF-8
" >> /etc/environment

export LANG \
LC_TIME \
LC_NAME \
LC_ADDRESS \
LC_NUMERIC \
LC_MEASUREMENT \
LC_MONETARY \
LC_TELEPHONE \
LC_PAPER \
LC_IDENTIFICATION \
LC_COLLATE \
LC_CTYPE


# Install Docker
groupadd --system --gid 999 docker
mv /etc/gshadow /etc/gshadow.orig
# enable passwdless primary group change via newgrp by removing the !
# from the corresponding gshadow entry
# later on needed by jenkins account so that Jenkins runs with primary group
# docker instead of its default primary group jenkins (needed so that a
# docker (client) command will be able to access the docker API socket
# unix:///var/run/docker.sock, since this socket is only accessable
# by user root and group docker)
sed 's/docker:!::/docker:::/' < /etc/gshadow.orig > /etc/gshadow
#echo "Fetching https://${DOCKER_BUCKET}/builds/Linux/x86_64/docker-${DOCKER_VERSION}.tgz"
echo "Fetching https://${DOCKER_BUCKET}/linux/static/${DOCKER_BRANCH}/x86_64/docker-${DOCKER_VERSION}.tgz"
#curl -fSL "https://${DOCKER_BUCKET}/builds/Linux/x86_64/docker-${DOCKER_VERSION}.tgz" -o docker.tgz
curl -fSL "https://${DOCKER_BUCKET}/linux/static/${DOCKER_BRANCH}/x86_64/docker-${DOCKER_VERSION}.tgz" -o docker.tgz
echo "${DOCKER_SHA256} *docker.tgz" | sha256sum -c -
tar -xzvf docker.tgz
mv docker/* /usr/local/bin/
rm -rf docker
rm docker.tgz
docker -v

# Install tini (non-configurable replacement for chaperone/supervisord)
# see the README.md available at
# https://github.com/krallin/tini
# for further information
curl -L https://github.com/krallin/tini/releases/download/v${TINI_VERSION}/tini_${TINI_VERSION}-amd64.deb -o tini_${TINI_VERSION}-amd64.deb
curl -L https://github.com/krallin/tini/releases/download/v${TINI_VERSION}/tini_${TINI_VERSION}-amd64.deb.asc -o tini_${TINI_VERSION}-amd64.deb.asc
export GNUPGHOME="$(mktemp -d)" && \
gpg --keyserver $GPG_KEYSERVER --recv-keys 595E85A6B1B4779EA4DAAEC70B588DFF0527A9B7 && \
gpg --batch --verify tini_${TINI_VERSION}-amd64.deb.asc tini_${TINI_VERSION}-amd64.deb && \
rm -r "$GNUPGHOME" tini_${TINI_VERSION}-amd64.deb.asc
# tini will be available as /usr/bin/tini
dpkg -i tini_${TINI_VERSION}-amd64.deb
rm tini_${TINI_VERSION}-amd64.deb

# Install gosu
curl -L -o /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$(dpkg --print-architecture)"
curl -L -o /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$(dpkg --print-architecture).asc"
export GNUPGHOME="$(mktemp -d)" && \
gpg --keyserver $GPG_KEYSERVER --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 && \
gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu && \
rm -r "$GNUPGHOME" /usr/local/bin/gosu.asc
chmod +x /usr/local/bin/gosu
#gosu nobody true

mkdir /etc/chaperone.d
cp -v /setup-baseimage/0*conf /etc/chaperone.d
