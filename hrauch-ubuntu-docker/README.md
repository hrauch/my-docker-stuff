Based on the (Official Docker
Image)[https://github.com/docker-library/docker.git], but augmented by the
packages contained in the hrauch-alpine-base image

Why not use the the hrauch-ubuntu-docker-dind image as tool image for
dockerfile builds from within an emphemeral slave?

Because it yielded strange errors during docker pull commands; only
docker version and docker login worked properly. The cause of these errors
is not yet known.

Why additional packages? Isn't this contradictory to the emphemerality
paradigm of Docker containers?

Yes, it is (to some extent). However, we want to be able to

* narrow down problems
* use Gradle (incl. the Gradle wrapper) and/or makefiles (deprecated) in order to avoid
duplication of awkward docker commands
* use the git executable in order to ensure that branches are tracking by
default (the Jenkins git plugin doesn't take care of this, even when
specifying check out to a specify local branch as an additional behavior).
