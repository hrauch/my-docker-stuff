FROM ubuntu:latest

MAINTAINER holger.rauch@posteo.de

ARG ARACHNI_VERSION=arachni-1.5.1-0.5.12

RUN export DEBIAN_FRONTEND=noninteractive && \
apt-get update && \
apt-get dist-upgrade -y && \
apt-get install -y apt-utils && \
apt-get install -y build-essential \
git \
curl \
libcurl4-openssl-dev \
wget \
libtimedate-perl \
libnet-ssleay-perl \
nmap \
python3-pip \
ruby \
ruby-dev \
ruby-childprocess \
ruby-nokogiri \
ruby-trollop \
zlib1g-dev \
libxml2-dev && \
rm -rf /var/lib/apt/lists/*

#libcurl4-nss-dev \
#libcurl4-gnutls-dev \
#libcurl4-openssl-dev \

# Install Gauntlt
RUN gem install gauntlt --no-rdoc --no-ri

# Install Attack tools
WORKDIR /opt

# arachni
RUN wget https://github.com/Arachni/arachni/releases/download/v1.5.1/${ARACHNI_VERSION}-linux-x86_64.tar.gz && \
    tar xzvf ${ARACHNI_VERSION}-linux-x86_64.tar.gz > /dev/null && \
    mv ${ARACHNI_VERSION} /usr/local && \
    ln -s /usr/local/${ARACHNI_VERSION}/bin/* /usr/local/bin/

# Nikto
RUN git clone --depth=1 https://github.com/sullo/nikto.git && \
    cd nikto/program && \
    echo "EXECDIR=/opt/nikto/program" >> nikto.conf && \
    ln -s /opt/nikto/program/nikto.conf /etc/nikto.conf && \
    chmod +x nikto.pl && \
    ln -s /opt/nikto/program/nikto.pl /usr/local/bin/nikto

# sqlmap
WORKDIR /opt
ENV SQLMAP_PATH /opt/sqlmap/sqlmap.py
RUN git clone --depth=1 https://github.com/sqlmapproject/sqlmap.git

# dirb
COPY vendor/dirb222.tar.gz dirb222.tar.gz

RUN tar xvfz dirb222.tar.gz > /dev/null && \
    cd dirb222 && \
    chmod 755 ./configure && \
    ./configure && \
    make && \
    ln -s /opt/dirb222/dirb /usr/local/bin/dirb

ENV DIRB_WORDLISTS /opt/dirb222/wordlists

# sslyze
RUN pip3 install sslyze
ENV SSLYZE_PATH /usr/local/bin/sslyze

ENTRYPOINT [ "/usr/local/bin/gauntlt" ]
