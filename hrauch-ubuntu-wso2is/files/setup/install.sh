#! /bin/sh
set -x

# env vars being passed from Dockerfile via ENV instruction,
# thus no need to individually assign them as $1, ...
[ -z $GIT_COMMIT ] && exit 1 || echo "GIT_COMMIT: ${GIT_COMMIT}"
[ -z $WSO2ISUSER ] && exit 1 || echo "WSO2ISUSER: ${WSO2ISUSER}"
[ -z $WSO2ISGROUP ] && exit 1 || echo "WSO2ISGROUP: ${WSO2ISGROUP}"
[ -z $WSO2ISVER ] && exit 1 || echo "WSO2ISVER: ${WSO2ISVER}"
[ -z $WSO2ISFULLVER ] && exit 1 || echo "WSO2ISFULLVER: ${WSO2ISFULLVER}"
[ -z $WSO2ISPACK ] && exit 1 || echo "WSO2ISVER: ${WSO2ISPACK}"
[ -z $WSO2UHOME ] && exit 1 || echo "WSO2UHOME: ${WSO2UHOME}"
[ -z $WSO2ISHOME ] && exit 1 || echo "WSO2ISHOME: ${WSO2ISHOME}"

[ -d /etc/chaperone.d ] && ls /etc/chaperone.d || exit 1
[ -f "/etc/chaperone.d/0*host*entries.conf" ] && rm -f "/etc/chaperone.d/0*host*entries.conf"
[ -x /usr/local/bin/chaperone ] || exit 1

groupadd --system ${WSO2ISGROUP} && \
useradd --system --create-home --home-dir ${WSO2UHOME} -g ${WSO2ISGROUP} ${WSO2ISUSER}

id -a ${WSO2ISUSER} || exit 1

#ls /setup-image/${WSO2ISPACK}.zip || exit 1
cd /setup-image
[ -d conf ] || exit 1
[ -d database ] || exit 1
[ -d data ] || exit 1
[ -d components ] || exit 1
# TODO: proxy settings (incl. proxy auth) supported during Docker build?
# If so, how?
#export http_proxy=http://host:port
#export https_proxy=http://host:port
#export no_proxy=".domain1.de .domain2.de .domain3.de localhost"

curl -o ${WSO2ISPACK}.zip -kL https://github.com/wso2/product-is/releases/download/v${WSO2ISFULLVER}/wso2is-${WSO2ISVER}.zip && \
unzip -qq ${WSO2ISPACK}.zip -d ${WSO2ISHOME} && \
cd ${WSO2ISHOME} && \
mv wso2is-${WSO2ISVER}/* . && rmdir  wso2is-${WSO2ISVER}
# backup original dirs from zip file
mv repository/conf repository/conf.orig
mv repository/database repository/database.orig
mv repository/data repository/data.orig
mv repository/components/lib repository/components/lib.orig

### copy preconfigured subdirs from our standalone installation

# XML files
cp -a /setup-image/conf repository
# registry (H2 database by default)
cp -a /setup-image/database repository
# internal LDAP stuff (.ldif files and such)
cp -a /setup-image/data repository
# additional JARs (Kerberos grant, Oracle JDBC drivers)
cp -a /setup-image/components/lib repository/components

echo $GIT_COMMIT > /etc/docker_image_version

#[ ! -d /etc/chaperone.d ] && mkdir /etc/chaperone.d
cp -av /setup-image/0*.conf /etc/chaperone.d
ls /etc/chaperone.d

chown -R ${WSO2ISUSER}:${WSO2ISGROUP} .

[ -d $WSO2ISHOME ] && chmod +x $WSO2ISHOME/bin/*.sh 

