WSO2 IS DL URL:

https://github.com/wso2/product-is/releases/download/v5.6.0-rc3/wso2is-5.6.0.zip

(Dieses .zip file und lokale repository Verzeichnisse nach files/setup
kopieren; alternativ könnte man repository auch als volume mounten)

Überblick über alle releases:

https://github.com/wso2/product-is/releases

Image bauen mit:

docker build -t wso2is:latest --compress --no-cache=true .

Starten mit:

docker run --rm --name wso2istest -it -p 9443:9443 --env-file env-vars <image_id_of_last_cmd_during_image_build_or_tag_name>

Konkretes Beispiel:

docker run --rm --name wso2istest -it -p 9443:9443 --env-file env-vars wso2is:latest

Healthcheck URL:

https://localhost:9443/carbon/admin/login.jsp

 
