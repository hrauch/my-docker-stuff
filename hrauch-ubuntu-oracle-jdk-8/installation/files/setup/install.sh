#! /bin/bash
set -x

GIT_COMMIT=$1

export DEBIAN_FRONTEND=noninteractive
export TZ=Europe/Berlin
export TERM=linux
export JAVA_HOME=/usr/lib/jvm/java-8-oracle

### Begin TEMP STUFF (until hrauch-ubuntu-base image is uploaded)

# required for apt-add-repository
# software-properties-common: required for 'apt-add-repository' tool
# dirmngr: required for GnuPG
# python3-pip, python3-setuptools: required for chaperone
# locales: required for locale-gen
# binutils: strings, nm cmds
# dnsutils: host, dig cmds
# procps, psmisc: /proc fs support
# net-tools: basic networking utils (netstat and such)
# curl: HTTP downloads
# less: pager
# jq: JSON parser for cmd line (useful for evaluating "docker inspect" output)
# file: determine file types
# vim-tiny,joe,nano: for editing text files
# git: needed as a docker dependency and also within Jenkins slave image
apt-get -qq -y update && apt-get -qq -y --no-install-recommends install \
dirmngr \
software-properties-common \
apt-transport-https \
binutils \
dnsutils \
python3-pip \
python3-setuptools \
locales \
make \
iputils-ping \
bash-completion \
zip \
unzip \
bzip2 \
procps \
psmisc \
daemon \
net-tools \
ca-certificates \
curl \
idn2 \
less \
jq \
file \
vim-tiny \
joe \
nano \
sudo \
openssh-client \
git

locale-gen en_US.UTF-8
locale-gen de_DE.UTF-8
dpkg-reconfigure locales

# upgrade pip using pip itself ;-)
pip3 install -U pip
pip3 install chaperone
    
echo $GIT_COMMIT > /etc/docker_image_version

chmod -c 666 /etc/hosts

echo "creating .bashrc for root user"
cat /setup-baseimage/dot-bashrc > /root/.bashrc

#echo "adding /etc/hosts entries"
# /tmp will contain the *writable* copy of /etc/hosts
#cp -av /etc/hosts /tmp
#cat /setup-baseimage/hosts >> /tmp/hosts
# modifying /etc/hosts directly prior from within a CMD/ENTRYPOINT doesn't
# work
# use ugly hack below => shamelessly stolen from
# http://jasonincode.com/customizing-hosts-file-in-docker/
# (alternative would be to set up a chaperone cfg file, but that's
# a bit overkill IMHO just for adding a few hosts ;-) to /etc/hosts)
#/bin/bash -c "cat /setup-baseimage/hosts >> /etc/hosts"
#mkdir -p /lib-override
#cp /lib/x86_64-linux-gnu/libnss_files-2.23.so /lib-override
#cp /usr/glibc-compat/lib/libnss_files-2.23.so /lib-override
# perl instead of sed for inplace editing (alternative would be ssed)
# IMPORTANT: Substituted path must be of exact same length, since we're
# modifying a text string inside a binary (.so file) here!
# (better be safe than sorry and patch *both* C libs)
#perl -pi -e 's:/etc/hosts:/tmp/hosts:g' /lib-override/libnss_files-2.23.so
#export LD_LIBRARY_PATH=/lib-override

echo "Copying chaperone config files"
mkdir /etc/chaperone.d
cp -av /setup-baseimage/*.conf /etc/chaperone.d

#echo "Copying Docker certs to /etc/docker/certs.d"
mkdir -p /etc/docker
#cp -av /setup-baseimage/etc/docker/* /etc/docker
ls -l /etc/docker

#echo "installing client ssh config files"
#mkdir ~/.ssh
#chmod -c 700 ~/.ssh
#cp -v /setup-baseimage/*rsa* ~/.ssh
#cp -v /setup-baseimage/config ~/.ssh
#cp -v /setup-baseimage/authorized_keys ~/.ssh

# see locale cmd for a complete list of locale related environment variables
# why not simply use LC_ALL? Because we still want english (error) messages
LANG=en_US.UTF-8
LC_TIME=de_DE.UTF-8
LC_NAME=de_DE.UTF-8
LC_ADDRESS=de_DE.UTF-8
LC_NUMERIC=de_DE.UTF-8
LC_MEASUREMENT=de_DE.UTF-8
LC_MONETARY=de_DE.UTF-8
LC_TELEPHONE=de_DE.UTF-8
LC_PAPER=de_DE.UTF-8
LC_IDENTIFICATION=de_DE.UTF-8
LC_COLLATE=de_DE.UTF-8
LC_CTYPE=de_DE.UTF-8

echo "
LANG=en_US.UTF-8
LC_TIME=de_DE.UTF-8
LC_NAME=de_DE.UTF-8
LC_ADDRESS=de_DE.UTF-8
LC_NUMERIC=de_DE.UTF-8
LC_MEASUREMENT=de_DE.UTF-8
LC_MONETARY=de_DE.UTF-8
LC_TELEPHONE=de_DE.UTF-8
LC_PAPER=de_DE.UTF-8
LC_IDENTIFICATION=de_DE.UTF-8
LC_COLLATE=de_DE.UTF-8
LC_CTYPE=de_DE.UTF-8
" >> /etc/environment

export LANG \
LC_TIME \
LC_NAME \
LC_ADDRESS \
LC_NUMERIC \
LC_MEASUREMENT \
LC_MONETARY \
LC_TELEPHONE \
LC_PAPER \
LC_IDENTIFICATION \
LC_COLLATE \
LC_CTYPE

### END temporary stuff (until hrauch-ubuntu-base image is uploaded)

apt-add-repository ppa:webupd8team/java
echo "debconf shared/accepted-oracle-license-v1-1 select true" | debconf-set-selections
apt-get -y -qq update
apt-get -y -qq install oracle-java8-installer

rm -rf $JAVA_HOME/*src.zip
rm -rf ${JAVA_HOME}/*/javaws
rm -rf ${JAVA_HOME}/*/jjs
#rm -rf ${JAVA_HOME}/*/keytool
rm -rf ${JAVA_HOME}/*/orbd
#rm -rf ${JAVA_HOME}/*/pack200
#rm -rf ${JAVA_HOME}/*/policytool
rm -rf ${JAVA_HOME}/*/rmid
rm -rf ${JAVA_HOME}/*/rmiregistry
rm -rf ${JAVA_HOME}/*/servertool
rm -rf ${JAVA_HOME}/*/tnameserv
#rm -rf ${JAVA_HOME}/*/unpack200
rm -rf ${JAVA_HOME}/*/*javafx*
rm -rf ${JAVA_HOME}/*/*jfx*
rm -rf ${JAVA_HOME}/*/amd64/libdecora_sse.so
rm -rf ${JAVA_HOME}/*/amd64/libfxplugins.so
rm -rf ${JAVA_HOME}/*/amd64/libglass.so
rm -rf ${JAVA_HOME}/*/amd64/libgstreamer-lite.so
rm -rf ${JAVA_HOME}/*/amd64/libjavafx*.so
rm -rf ${JAVA_HOME}/*/amd64/libjfx*.so
rm -rf ${JAVA_HOME}/*/amd64/libprism_*.so
rm -rf ${JAVA_HOME}/*/deploy*
rm -rf ${JAVA_HOME}/*/desktop
rm -rf ${JAVA_HOME}/*/ext/jfxrt.jar
rm -rf ${JAVA_HOME}/*/ext/nashorn.jar
rm -rf ${JAVA_HOME}/*/javaws.jar
rm -rf ${JAVA_HOME}/*/jfr
rm -rf ${JAVA_HOME}/*/jfr
rm -rf ${JAVA_HOME}/*/jfr.jar
rm -rf ${JAVA_HOME}/*/missioncontrol
rm -rf ${JAVA_HOME}/*/oblique-fonts
rm -rf ${JAVA_HOME}/*/plugin.jar
rm -rf ${JAVA_HOME}/*/visualvm
rm -rf ${JAVA_HOME}/man
rm -rf ${JAVA_HOME}/plugin
rm -rf ${JAVA_HOME}/*.txt
rm -rf ${JAVA_HOME}/*/*/javaws
rm -rf ${JAVA_HOME}/*/*/jjs
#rm -rf ${JAVA_HOME}/*/*/keytool
rm -rf ${JAVA_HOME}/*/*/orbd
#rm -rf ${JAVA_HOME}/*/*/pack200
rm -rf ${JAVA_HOME}/*/*/policytool
rm -rf ${JAVA_HOME}/*/*/rmid
rm -rf ${JAVA_HOME}/*/*/rmiregistry
rm -rf ${JAVA_HOME}/*/*/servertool
rm -rf ${JAVA_HOME}/*/*/tnameserv
#rm -rf ${JAVA_HOME}/*/*/unpack200
rm -rf ${JAVA_HOME}/*/*/*javafx*
rm -rf ${JAVA_HOME}/*/*/*jfx*
rm -rf ${JAVA_HOME}/*/*/amd64/libdecora_sse.so
rm -rf ${JAVA_HOME}/*/*/amd64/libfxplugins.so
rm -rf ${JAVA_HOME}/*/*/amd64/libglass.so
rm -rf ${JAVA_HOME}/*/*/amd64/libgstreamer-lite.so
rm -rf ${JAVA_HOME}/*/*/amd64/libjavafx*.so
rm -rf ${JAVA_HOME}/*/*/amd64/libjfx*.so
rm -rf ${JAVA_HOME}/*/*/amd64/libprism_*.so
rm -rf ${JAVA_HOME}/*/*/deploy*
rm -rf ${JAVA_HOME}/*/*/desktop
rm -rf ${JAVA_HOME}/*/*/ext/jfxrt.jar
rm -rf ${JAVA_HOME}/*/*/ext/nashorn.jar
rm -rf ${JAVA_HOME}/*/*/javaws.jar
rm -rf ${JAVA_HOME}/*/*/jfr
rm -rf ${JAVA_HOME}/*/*/jfr
rm -rf ${JAVA_HOME}/*/*/jfr.jar
rm -rf ${JAVA_HOME}/*/*/missioncontrol
rm -rf ${JAVA_HOME}/*/*/oblique-fonts
rm -rf ${JAVA_HOME}/*/*/plugin.jar
rm -rf ${JAVA_HOME}/*/*/visualvm
rm -rf ${JAVA_HOME}/*/man
rm -rf ${JAVA_HOME}/*/plugin
rm -rf ${JAVA_HOME}/*.txt

apt-get -y -qq install oracle-java8-set-default
apt-get -y -qq install oracle-java8-unlimited-jce-policy

export JAVA_HOME=/usr/lib/jvm/java-8-oracle
export PATH=$JAVA_HOME/bin:$PATH
export MANPATH=$JAVA_HOME/man:$MANPATH

which javac || exit 1
which java || exit 1
which keytool || exit 1
which jar || exit 1
java -version || exit 1

# add public cert from Bitbucket server to JDK system trust store
# just left in there for reference
#curl -o /tmp/SSLPoke.class https://confluence.atlassian.com/kb/files/779355358/779355357/1/1441897666313/SSLPoke.class
#cd /tmp
#java SSLPoke bitbucket.mydomain 8443 | grep -c "PKIX path building failed" && echo "SSL connection failed (bitbucket.mydomain pub cert not yet imported in JDK trust store)"
#openssl s_client -connect bitbucket.mydomain:8443 < /dev/null | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > /tmp/bitbucket.mydomain_public.crt
#keytool -import -alias bitbucket.mydomain -keystore $JAVA_HOME/jre/lib/security/cacerts -storepass changeit -noprompt -file /tmp/bitbucket.mydomain_public.crt
#keytool -list -alias bitbucket.mydomain -keystore $JAVA_HOME/jre/lib/security/cacerts -storepass changeit -noprompt | grep -c "bitbucket.mydomain" && echo "Bitbucket server pub cert imported in JDK trust store"
#java SSLPoke bitbucket.mydomain 8443 | grep -c "Successfully connected" && echo "SSL connection to Bitbucket server OK"

echo $GIT_COMMIT > /etc/docker_image_version

[ ! -d /etc/chaperone.d ] && mkdir /etc/chaperone.d
cp -av /setup-baseimage/0*.conf /etc/chaperone.d

