Oracle JDK 8 for Ubuntu LTS (image is based on hrauch-ubuntu-docker)

Why use Ubuntu LTS at all when Alpine is almost half the size?

Mainly for
* having an official LTS distro but with more current SW than Debian stable
* using the Debian package management system to the fullest extent possible
(thus preventing manual downloads for JDK, JCE)
* Full support for the official Docker install.sh script at [stable Docker web site] (http://get.docker.com).
(thus always retrieving the **latest stable** version; contrary to Alpine which
maintains own Docker packages in their `edge` repository)
