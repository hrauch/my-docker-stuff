#! /bin/bash

# convenience script as a substitute for docker docker compose
# $1: <image_id>
# $2: name

[[ "$#" -lt 2 ]] && echo "Need at least 2 params: <image_id> and container name (a 3rd param can be specified for the command to be executed in the container)" && exit 1

#docker create -v /jenkinsdata --name jenkinsdata docker.repo.host/hrauch-alpine-jenkins-cli /bin/true
#docker run --rm --privileged --name $2 -it --volumes-from jenkinsdata -p 8090:8080 -p 50000:50000 $1
docker run --rm --privileged --name $2 -it $1 $3
