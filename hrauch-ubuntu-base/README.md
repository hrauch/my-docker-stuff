hrauch-ubuntu-base
===============

`Ubuntu` variant of my Alpine base distribution.

# Purpose

see `README.md` for `hrauch-alpine-base`

**Additional reasons:**

- LTS variant available with more recent base software than e.g. CentOS or Debian
- officially supported distribution by http://get|test|experimental.docker.com install script (main advantage of this script is that regular package management tools are used to add Docker, thus Docker is cleanly integrated in the distribution)
- clean integration of Oracle JDK using standard package management tools via Ubuntu Launchpad
- For Linux kernel versions >= 4.4 `devicemapper` is automatically used in Debian based distributions (initialized at `dockerd` startup via `systemd`); although not as an LVM based `thinpool`, but "only" using loop devices). This is a great advantage compared to CentOS, where `devicemapper` setup is not supported as part of installing the `docker` package via `yum`)

However, it should be noted that there's a drawback too:
- resulting image size is twice that of the corresponding Alpine image

# Included Tools/Packages

The purpose of the installed packages is the same as that of the corresponding Alpine variant, but the package names differ slightly.

| Package Name  | Purpose     |
| ------------- |-------------|
| `software-properties-common` | required for `apt-add-repository` tool |
|  `python3-pip`, `python3-setuptools` | prerequisites for [`chaperone`](https://github.com/garywiz/chaperone) (lightweight process manager) |
| `binutils` | `strings` and `nm` commands (for inspecting binaries) |
| `dnsutils` | `host`, `dig` command |
| `procps`, `psmisc` | support for `procfs` and `ps` command |
| `net-tools` | basic network utilities like `netstat` and such |
| `curl` | downloading stuff via various protocols (more powerful than `wget`) |
| `less` | pager |
| `jq` | json query" JSON parser for command line (useful for parsing e.g. `docker inspect` output) |
| `openssh-client` | `ssh` **client** command (server part omitted on purpose) |
| `file` | displaying file/MIME type info |
| `vim-tiny` | for editing text files (possible alternatives if it's considered too "fat": `nano`, `joe`) |


# `dockerfile`

see `README.md` for `hrauch-alpine-base`

# `makefile`

see `README.md` for `hrauch-alpine-base`
