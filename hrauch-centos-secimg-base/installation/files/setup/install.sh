#! /bin/bash
#set -o errexit
set -e
set -o pipefail
set -x

#export DEBIAN_FRONTEND=noninteractive
[ -z $GIT_COMMIT ] && exit 1 || echo "GIT_COMMIT=${GIT_COMMIT}"
[ -z $ARACHNI_BASE_VERSION ] && exit 1 || echo "ARACHNI_BASE_VERSION=${ARACHNI_BASE_VERSION}"
[ -z $ARACHNI_VERSION ] && exit 1 || echo "ARACHNI_VERSION=${ARACHNI_VERSION}"

export TZ=Europe/Berlin

echo "Europe/Berlin" > /etc/TZ
echo "TZ=Europe/Berlin" >> /etc/environment

# dirmngr: required for GnuPG
# python3-pip, python3-setuptools: required for chaperone
# locales: required for locale-gen
# binutils: strings, nm cmds
# dnsutils: host, dig cmds
# procps, psmisc: /proc fs support
# net-tools: basic networking utils (netstat and such)
# curl: HTTP downloads
# less: pager
# jq: JSON parser for cmd line (useful for evaluating "docker inspect" output)
# file: determine file types
# vim-tiny,joe,nano: for editing text files
# git: needed as a docker dependency and also within Jenkins slave image
#apt-get -qq -y update && apt-get -qq -y --no-install-recommends install \
yum -y update
yum -y install yum-plugin-priorities yum-utils
#yum -y groupinstall development
yum -y install centos-release-scl-rh centos-release-scl
yum -y install https://centos7.iuscommunity.org/ius-release.rpm
#yum -y install python36u python36u-pip python36u-devel
yum install -y \
gcc \
make \
gawk \
curl \
libcurl-devel \
wget \
libxml2-devel \
zlib-devel \
nmap \
python36u \
python36u-pip \
python36u-devel \
rh-ruby23 \
rh-ruby23-ruby-devel \
perl-TimeDate \
perl-Net-SSLeay

yum clean all
rm -rf /var/cache/yum

> /etc/profile.d/rh-ruby23.sh <<EOF
#!/bin/bash
source /opt/rh/rh-ruby23/enable
export X_SCLS="$(scl enable rh-ruby23 'echo $X_SCLS')"
EOF

chmod +x /etc/profile.d/rh-ruby23.sh

[ -x /etc/profile.d/rh-ruby23.sh ] && cat /etc/profile.d/rh-ruby23.sh

locale
#locale-gen en_US.UTF-8
#locale-gen de_DE.UTF-8
#localectl ...

#[ -x /etc/profile.d/rh-ruby23.sh ] && . /etc/profile.d/rh-ruby23.sh || exit 1
#scl enable rh-ruby23 bash
[ -f /opt/rh/rh-ruby23/enable ] && . /opt/rh/rh-ruby23/enable || exit 1

ruby --version
gem --version

python3.6 -V
pip3.6 --version

# sslyze
pip3.6 install sslyze

cd /usr/local

# Arachni
wget https://github.com/Arachni/arachni/releases/download/v${ARACHNI_BASE_VERSION}/${ARACHNI_VERSION}-linux-x86_64.tar.gz
tar xzvf ${ARACHNI_VERSION}-linux-x86_64.tar.gz > /dev/null
ln -s /usr/local/${ARACHNI_VERSION}/bin/* /usr/local/bin/

# Nikto
git clone --depth=1 https://github.com/sullo/nikto.git
echo "EXECDIR=/usr/local/nikto/program" >> nikto/program/nikto.conf
ln -s /usr/local/nikto/program/nikto.conf /etc/nikto.conf
chmod +x nikto/program/nikto.pl
ln -s /usr/local/nikto/program/nikto.pl /usr/local/bin/nikto

# sqlmap
git clone --depth=1 https://github.com/sqlmapproject/sqlmap.git
ln -s /usr/local/sqlmap/sqlmap.py /usr/local/bin/sqlmap

# check tools
sslyze --version
arachni --version
nikto --version
sqlmap --version
    
echo $GIT_COMMIT > /etc/docker_image_version
